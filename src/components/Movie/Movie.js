import React, {Component, Fragment} from 'react';

const Movie = props => {
    return (
        <div className="Movie">
            <input
                type="text"
                value={props.title}
                onChange={props.currentMovieName}
                onBlur={props.unfocused}
            />
            <button onClick={props.delete}>Delete Movie Item</button>
        </div>
    )
};

export default Movie;
