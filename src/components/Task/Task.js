import React from 'react';
import './Task.css';

const Task = props =>  (
    <div className="Task">
        <h3>{props.title}</h3>
        <button className="deleteTaskBtn" onClick={props.deleted}>Delete the task</button>
    </div>
);

export default Task;
