import React,{Component} from 'react';
import './MovieAdd.css';
import FontAwesome from 'react-fontawesome';


import axios from 'axios';

class MovieAdd extends Component {
    state = {
        movie: {title: ''},
        loading: false
    };

    movieCreateHandler = event => {
        event.preventDefault();

        this.setState({loading: true});

        axios.post('/movies.json', this.state.movie).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/movieList');
        })
    };

    titleChanged = event => {
        const movie = {title: event.target.value};
        this.setState({movie});
    };

    render() {
        if(this.state.loading){
            return <FontAwesome
                className='super-crazy-colors'
                name='rocket'
                size='2x'
                spin
                style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
            />
        } else {
            return (
                <form className='AddForm'>
                    <input type="text"
                           name="title"
                           placeholder='Enter title'
                           value={this.state.movie.title}
                           onChange={this.titleChanged}
                    />
                    <button onClick={this.movieCreateHandler}>Create movie item</button>
                </form>
            )
        }

    }
}

export default MovieAdd;