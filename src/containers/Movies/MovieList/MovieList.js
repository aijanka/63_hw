import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Movie from "../../../components/Movie/Movie";

class MovieList extends Component {
    state = {
        movies: []
    };

    componentDidMount() {
        this.getMovies();
    }

    getMovies = () => {
        axios.get('/movies.json').then(response => {
            console.log(response.data)
            const movies = response.data;
            // const movies = Object.keys(response.data).map(key => ({title: response.data[key].title, id: key}));
            this.setState({movies})
        })
    }

    titleChangeHandler = (event, id) => {
        const movies = {...this.state.movies};
        movies[id] = {title: event.target.value};
        console.log(movies)
        this.setState({movies});
    };

    deleteMovieHandler = id => {
        axios.delete(`/movies/${id}.json`).then(() => {
            this.getMovies();
        })
    }

    sendTitleToServer = (event, id) => {
        axios.put(`/movies/${id}.json`, this.state.movies[id]);
    };

    render() {

        const emptyList = <h1>No movies yet</h1>;
        if(this.state.movies) {
            const movieList = Object.keys(this.state.movies).map(movieId => {
                const movie = this.state.movies[movieId];
                return <Movie
                    title={movie.title}
                    id={movieId}
                    currentMovieName={(event) => this.titleChangeHandler(event, movieId)}
                    unfocused={() => this.sendTitleToServer(movieId)}
                    delete={() => this.deleteMovieHandler(movieId)}
                    key={movieId} />
                }
            );
            return movieList;
        } else {
            return emptyList;
        }
    }
}

export default MovieList;