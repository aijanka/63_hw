import React, {Component, Fragment} from 'react';
import Task from "../../../components/Task/Task";
import axios from 'axios';

class TasksList extends Component {
    state = {
        tasks: []
    };

    componentDidMount() {
        axios.get('/tasks.json').then(response => {
            const tasks = [];

            for(let key in response.data) {
                tasks.push({...response.data[key], id: key});
            }
            this.setState({tasks});
        })
    }

    deleteTaskHandler = id => {
        axios.delete(`/tasks/${id}.json`).then(() => {
            this.setState(prevState => {
                const tasks = [...prevState.tasks];
                const index = tasks.findIndex(task => task.id === id);
                tasks.splice(index, 1);
                console.log(prevState);
                return {tasks};
            })
        })
    };

    render() {
        if(this.state.tasks.length !== 0){
            return (
                <Fragment>

                    <div className='TaskList'>
                        {this.state.tasks.map(task => (<Task
                                                        title={task.title}
                                                        key={task.id}
                                                        deleted={() => this.deleteTaskHandler(task.id)}
                                                        />))}
                    </div>
                </Fragment>

            )
        } else {
            return <h1 className='EmptyListForm'>Task list is empty</h1>;
        }

    }
}

export default TasksList;
