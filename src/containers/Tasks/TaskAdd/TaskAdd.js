import React,{Component} from 'react';
import './TaskAdd.css';
import FontAwesome from 'react-fontawesome';


import axios from 'axios';

class TaskAdd extends Component {
    state = {
        task: {title: ''},
        loading: false
    };

    taskCreateHandler = event => {
        event.preventDefault();

        this.setState({loading: true});

        axios.post('/tasks.json', this.state.task).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/taskList');
        })
    };

    titleChanged = event => {
        const task = {title: event.target.value};
        this.setState({task});
    };

    render() {
        if(this.state.loading){
            return <FontAwesome
                className='super-crazy-colors'
                name='rocket'
                size='2x'
                spin
                style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
            />
        } else {
            return (
                <form className='AddForm'>
                    <input type="text"
                           name="title"
                           placeholder='Enter title'
                           value={this.state.task.title}
                           onChange={this.titleChanged}
                    />
                    <button onClick={this.taskCreateHandler}>Create task</button>
                </form>
            )
        }

    }
}

export default TaskAdd;