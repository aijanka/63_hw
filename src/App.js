import React, { Component, Fragment } from 'react';
import TasksList from "./containers/Tasks/TasksList/TasksList";
import {Switch, Route, NavLink} from "react-router-dom";
import TaskAdd from "./containers/Tasks/TaskAdd/TaskAdd";
import './NavForm.css';
import MovieList from "./containers/Movies/MovieList/MovieList";
import MovieAdd from "./containers/Movies/MovieAdd/MovieAdd";

class App extends Component {
  render() {
    return (
        <Fragment>
            <nav><ul className='NavForm'>
                <li className='primaryLi'>Tasks
                    <ul>
                        <li><NavLink to='/taskList'>View task list</NavLink></li>
                        <li><NavLink to='/addTask' exact>Add new task</NavLink></li>
                    </ul>
                </li>
                <li className='primaryLi'>Movies
                    <ul>
                        <li><NavLink to='/movieList'>View movies list</NavLink></li>
                        <li><NavLink to='/addMovie'>Add new movie</NavLink></li>
                    </ul>
                </li>
                <li></li>
            </ul></nav>
            <Switch>
                <Route path='/taskList' exact component={TasksList}/>
                <Route path='/addTask' component={TaskAdd}/>
                <Route path='/movieList' exact component={MovieList}/>
                <Route path='/addMovie' component={MovieAdd}/>
                <Route render={() => <h1> Not Found </h1>}/>
            </Switch>
        </Fragment>

    );
  }
}

export default App;
